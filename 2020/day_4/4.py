import sys
import re

batch = open(sys.argv[1], "r").read().split("\n")
batch.append("\n")

passports = []

fields = [
    "byr",
    "iyr",
    "eyr",
    "hgt",
    "hcl",
    "ecl",
    "pid",
    "cid"
]

valid_pass_count = 0
all_fields = ""

class Passport:
    def __init__(self, data_str):
        self.parse(data_str)

    def parse(self, data_str):
        fields = list(filter(None, data_str.split(' ')))
        self.attrs = {}
        for attr in fields:
            key, val = attr.split(":")
            self.attrs[key] = val

    def has_byr(self):
        return 'byr' in self.attrs
    def has_iyr(self):
        return 'iyr' in self.attrs
    def has_eyr(self):
        return 'eyr' in self.attrs
    def has_hgt(self):
        return 'hgt' in self.attrs
    def has_hcl(self):
        return 'hcl' in self.attrs
    def has_ecl(self):
        return 'ecl' in self.attrs
    def has_pid(self):
        return 'pid' in self.attrs
    def has_cid(self):
        return 'cid' in self.attrs

    def byr_valid(self):
        if not self.has_byr():
            return False
        value = self.attrs['byr']
        return re.match("^\d{4}$", value) and int(value) >= 1920 and int(value) <= 2002
    def iyr_valid(self):
        if not self.has_iyr():
            return False
        value = self.attrs['iyr']
        return re.match("^\d{4}$", value) and int(value) >= 2010 and int(value) <= 2020
    def eyr_valid(self):
        if not self.has_eyr():
            return False
        value = self.attrs['eyr']
        return re.match("^\d{4}$", value) and int(value) >= 2020 and int(value) <= 2030
    def hgt_valid(self):
        if not self.has_hgt():
            return False
        value = self.attrs['hgt']

        if not re.match("^(\d+)(cm|in)$", value):
            return False

        if re.match("(\d+)cm", value):
            number = re.match("^(\d+)cm$", value)[1]
            if int(number) >= 150 and int(number) <= 193:
                return True
            else:
                return False
        else:
            number = re.match("^(\d+)in$", value)[1]
            if int(number) >= 59 and int(number) <= 76:
                return True
            else:
                return False
    def hcl_valid(self):
        if not self.has_hcl():
            return False
        value = self.attrs['hcl']
        return re.match("^#[0-9a-f]{6}$", value)
    def ecl_valid(self):
        if not self.has_ecl():
            return False
        value = self.attrs['ecl']
        return value in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
    def pid_valid(self):
        if not self.has_pid():
            return False
        value = self.attrs['pid']
        return re.match("^\d{9}$", value)
    def cid_valid(self):
        return True

    def invalid_reason(self):
        reasons = []
        if not self.byr_valid():
            reasons.append('byr')
        if not self.iyr_valid():
            reasons.append('iyr')
        if not self.eyr_valid():
            reasons.append('eyr')
        if not self.hgt_valid():
            reasons.append('hgt')
        if not self.hcl_valid():
            reasons.append('hcl')
        if not self.ecl_valid():
            reasons.append('ecl')
        if not self.pid_valid():
            reasons.append('pid')
        if not self.cid_valid():
            reasons.append('cid')

        if not reasons:
            return None
        else:
            return reasons

    def valid(self):
        if self.invalid_reason():
            return False
        else:
            return True
        # return bool(self.byr_valid() and self.iyr_valid() and self.eyr_valid() and self.hgt_valid() and self.hcl_valid() and self.ecl_valid() and self.pid_valid())

valid_pass_count = 0

def test():
    p = Passport("pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1919 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:2003 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1920 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("Ture:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:74in ecl:grn iyr:2009 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:74in ecl:grn iyr:2021 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:74in ecl:grn iyr:2010 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("Ture:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:74in ecl:grn iyr:2020 eyr:2019 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:74in ecl:grn iyr:2020 eyr:2031 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:74in ecl:grn iyr:2020 eyr:2020 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("Ture:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:58in ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:77in ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:59in ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("True:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:149cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:194cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:150cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("True:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:193cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:193cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2fa cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:193cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2g cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:193cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("True:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:193cm ecl:bla iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:193cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("True:", p.invalid_reason())

    p = Passport("pid:a87499704 hgt:193cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:07499704 hgt:193cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:0874997040 hgt:193cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("False:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:193cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f cid:bla")
    print(p.__dict__)
    print("True:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:193cm ecl:grn iyr:2020 eyr:2030 byr:2002 hcl:#623a2f")
    print(p.__dict__)
    print("True:", p.invalid_reason())

    p = Passport("pid:087499704 hgt:193cm ecl:grn eyr:2030 byr:2002 hcl:#623a2f")
    print(p.__dict__)
    print("False:", p.invalid_reason())


# test()

passports = []
valid = []

for l in batch:
    if l:
        all_fields += " " + l
    else:
        p = Passport(all_fields)
        all_fields = ''
        passports.append(p)

        if p.valid():
            valid.append(p)
            valid_pass_count += 1

print(valid_pass_count)
