import sys
import numpy

topology = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

height = len(topology)
width = len(topology[0])
# print(height, width)

def slope_trees(x_sl, y_sl, topology):
    x, y, trees = 0, 0, 0
    while y <= height - 1 - y_sl:
       x = (x + x_sl) % width
       y = y + y_sl
       # print("X: ", x, "Y: ", y, "space:", topology[y][x])
       if topology[y][x] == '#':
           trees += 1
    return trees

slopes = [
    [1, 1],
    [3, 1],
    [5, 1],
    [7, 1],
    [1, 2]
]

trees = list(map(lambda sl: slope_trees(*sl, topology), slopes))

print(numpy.prod(trees))
