import sys
import string
import re

rules = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

data = {}

for r in rules:
    res = re.match('^(.*) bags contain (.*)$', r)
    color = res[1]
    content = {}
    for c in res[2].split(', '):
        if c != 'no other bags.':
            mat = re.match('^(\d)+ (.*) bags*', c)
            # print(mat[1])
            # print(mat[2])
            content[mat[2]] = int(mat[1])
    data[color] = content


# print(data)

def contain_color(color):
    include = []
    for i in data:
        if color in data[i]:
            include.append(i)
    return include


total = 0
new_total = -1
new_colors = []
colors = ['shiny gold']

while total != new_total:
    # print(new_total)
    total = new_total
    for c in colors:
        new_colors += contain_color(c)
    colors = new_colors
    # print(new_colors)
    new_colors = list(set(new_colors))
    new_total = len(new_colors)
print(len(new_colors))

total = 0
def nested_count(bag):
    total_nested = 0
    nested = data[bag]
    print( bag, " =>", nested)
    for b in nested:
        nested_c = nested[b]
        total_nested += nested_c * (nested_count(b) + 1)
    return total_nested

print(nested_count('shiny gold'))

