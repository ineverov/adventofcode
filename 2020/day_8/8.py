import sys
import copy

program = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

class Program:
    def __init__(self, commands):
        self.commands = commands
        # print(self.__dict__)

    def parse(self, commands):
        for c in commands:
            cmd, arg = c.split(' ')
            self.commands.append([cmd, int(arg)])

    def run(self, acc=0):
        self.memory = [[c[0], c[1], False] for c in self.commands]
        self.address = 0
        self.acc = acc
        self.halt = False
        # print(self.memory)

        while True:
            if self.address >= len(self.memory):
                break

            command = self.memory[self.address]
            print(f"Address: {self.address}, ACC: {self.acc}, Command: {command[0]}, Args: {command[1]}")

            if command[2]:
                self.halt = True
                break

            case = {
                'acc': self.do_acc,
                'nop': self.do_nop,
                'jmp': self.do_jmp
            }
            command[2] = True
            case[command[0]](command[1])

    def do_acc(self, arg):
        self.acc += arg
        self.address += 1

    def do_nop(self, arg):
        self.address += 1

    def do_jmp(self, arg):
        self.address += arg


def parse(cmds):
    commands = []
    for c in cmds:
        cmd, arg = c.split(' ')
        commands.append([cmd, int(arg)])
    return commands

commands = parse(program)

jmps = [ ind for ind, c in enumerate(commands) if c[0] == 'jmp' ]
# jmps.reverse()
nops = [ ind for ind, c in enumerate(commands) if c[0] == 'nop' ]
# nops.reverse()

def fix():
    for address in jmps:
        cmds_copy = copy.deepcopy(commands)
        cmds_copy[address][0] = 'nop'
        print(f"Flipping jmp to nop at address {address}")
        p = Program(cmds_copy)
        p.run()

        if not p.halt:
            return address, p.acc

    for address in nops:
        cmds_copy = copy.deepcopy(commands)
        cmds_copy[address][0] = 'jmp'
        print(f"Flipping nop to jmp at address {address}")
        p = Program(cmds_copy)
        p.run()

        if not p.halt:
            return address, p.acc

print(fix())
