import sys
from collections import OrderedDict as odict

adapters = [0]

adapters += [ int(n) for n in list(filter(None, open(sys.argv[1], "r").read().split("\n"))) ]

adapters.sort()
adapters.append(adapters[-1] + 3)

# print(adapters)

diffs = {
    1: 0,
    2: 0,
    3: 0
}

for i, x in enumerate(adapters[:-1]):
    diff = adapters[i+1] - x
    # print(diff)
    diffs[diff] += 1

number = diffs[1] * diffs[3]
print(diffs)
print(f"Part 1: {number}")

jolts = adapters

dag = odict([(x, {y for y in range(x+1, x+4) if y in jolts}) for x in jolts])
print(jolts)
print(dag)

def dfc(D, v, M={}):
    "Memoized depth first counter"
    if v in M:
        return M[v]
    elif D[v]:
        M[v] = sum(dfc(D, x, M) for x in D[v])
        return M[v]
    else:
        return 1

print(dfc(dag, 0))
