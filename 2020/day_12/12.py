import sys

instructions = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

orientations = ['E', 'S', 'W', 'N']
route = []
current_orientation = 'E'
current_pos = [0, 0]

def parse(commands):
    instr = []
    for c in commands:
        instr.append([c[0], int(c[1:])])
    return instr

# print(parse(instructions))

def perform(command, arg, current_pos, current_orientation):
    print(f"Command {command}, Arg: {arg}")
    if command == 'N':
        current_pos[0] -= arg
    elif command == 'S':
        current_pos[0] += arg
    elif command == 'E':
        current_pos[1] += arg
    elif command == 'W':
        current_pos[1] -= arg
    elif command == 'L':
        shift = int(arg / 90)
        current_ind = orientations.index(current_orientation)
        current_orientation = orientations[(current_ind - shift)%4]
    elif command == 'R':
        shift = int(arg / 90)
        current_ind = orientations.index(current_orientation)
        current_orientation = orientations[(current_ind + shift) % 4]
    elif command == 'F':
        if current_orientation == 'E':
            current_pos[1] += arg
        elif current_orientation == 'W':
            current_pos[1] -= arg
        elif current_orientation == 'N':
            current_pos[0] -= arg
        elif current_orientation == 'S':
            current_pos[0] += arg
    print(f"Current pos: {current_pos}, Orientation: {current_orientation}")

    route.append(current_pos)
    return current_pos, current_orientation

instrs = parse(instructions)
for i in instrs:
    current_pos, current_orientation = perform(i[0], i[1], current_pos, current_orientation)

print(abs(current_pos[0])+ abs(current_pos[1]))

current_pos = [0,0]
waypoint = [-1, 10]

def waypoint_perform(command, arg, current_pos, waypoint):
    print(f"Command {command}, Arg: {arg}")
    if command == 'N':
        waypoint[0] -= arg
    elif command == 'S':
        waypoint[0] += arg
    elif command == 'E':
        waypoint[1] += arg
    elif command == 'W':
        waypoint[1] -= arg
    elif command == 'L':
        shift = int(arg / 90)
        for i in range(shift):
            waypoint = [-waypoint[1], waypoint[0]]
    elif command == 'R':
        shift = int(arg / 90)
        for i in range(shift):
            waypoint = [waypoint[1], -waypoint[0]]
    elif command == 'F':
        current_pos = [current_pos[0] + arg * waypoint[0], current_pos[1] + arg * waypoint[1]]

    print(f"Current pos: {current_pos}, Waypoint: {waypoint}")

    route.append(current_pos)
    return current_pos, waypoint

for i in instrs:
    current_pos, waypoint = waypoint_perform(i[0], i[1], current_pos, waypoint)

print(abs(current_pos[0])+ abs(current_pos[1]))
