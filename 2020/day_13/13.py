import sys
import math

inputs = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

timestamp = int(inputs[0])
buses = inputs[1].split(',')

closest = 0
earliest = None

for bus in buses:
    if bus == 'x':
        continue

    number = int(bus)

    decimal = (timestamp % number) / number

    if decimal > closest:
        closest = decimal
        earliest = number

next_time = math.ceil(timestamp / earliest) * earliest
print(f"Next bus: {earliest} at #{next_time} in {next_time - timestamp}")
print(f"ID {(next_time - timestamp)* earliest}")

print("PART II")

from functools import reduce
def chinese_remainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a*b, n)
    print(a)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        print(f"Ni {n_i} Ai {a_i} ProdI {p} Euclid {mul_inv(p, n_i)}")
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod


def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1: return 1
    while a > 1:
        q = a // b
        a, b = b, a%b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0: x1 += b0
    return x1


offsets = [int(b) - i for i,b in enumerate(buses) if b != 'x']
print(offsets)
bus_ids = [int(b) for b in buses if b != 'x']
print(f'Part 2: {chinese_remainder(bus_ids, offsets)}')
