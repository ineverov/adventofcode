input = File.read(ARGV[0]).split[1].split(',')

buses = input.each_with_index.each_with_object({}) { |(n, i), h| h[n.to_i] = n.to_i - i if n != 'x' }

i = buses.size
A = buses.keys
R = buses.values

M = A.inject(&:*)

N = A.map { |ai| M / ai }

puts A.inspect
puts R.inspect
# puts M
puts N.inspect

def euclidian(a, b)
  x = [a, 1, 0]
  y = [b, 0, 1]

  while y[0] != 0
    q = x[0] / y[0]
    r = 3.times.map { |ind| x[ind] - q * y[ind] }

    x = y
    y = r
    puts "X #{x} Y #{y}"
  end
  x[0]

  b0 = b
  x0, x1 = 0, 1
  return 1 if b == 1
  while a > 1
      q = a / b
      a, b = b, a % b
      x0, x1 = x1 - q * x0, x0
      puts "A #{a} B #{b} X0 #{x0} X1 #{x1}"
  end
  x1 += b0 if x1 < 0
  x1
end


D = i.times.map { |ind| euclidian(N[ind], A[ind]) }

puts N.inspect
puts D.inspect

sum = 0
i.times do |ind|
  puts("A #{A[ind]} N #{R[ind]} ProdI #{N[ind]} Euclid #{D[ind]}")
  sum += R[ind]*D[ind]*N[ind]
end

puts sum % M
