import sys

passes = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

# FBFBBFF RLR
# 0101100 101

def get_id(p):
    row = p[:7]
    column = p[7:]
    row = row.replace('F','0')
    row = row.replace('B', '1')
    column = column.replace('R', '1')
    column = column.replace('L', '0')
    row = int(row, 2)
    column = int(column, 2)
    return row * 8 + column




ids = []
for p in passes:
    ids.append(get_id(p))
min_id, max_id = min(ids), max(ids)

ids.sort()

i = 0
while min_id + i < max_id:
    i+=1
    # print(ids[i], min_id + i)
    if ids[i] != min_id + i:
        print(min_id + i)
        break
