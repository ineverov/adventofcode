import sys
import string

data = open(sys.argv[1], "r").read().split("\n")

total = 0
ans = ''

for l in data:
    if l:
        ans += l
    else:
        total += len(list(set(ans.replace("\W", ''))))
        ans = ''

print(total)

common = list(string.ascii_lowercase)

print(common)

total = 0

def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3

for l in data:
    if l:
        common = intersection(common, list(l))
        print(common)
    else:
        print("End of group")
        print(len(common))
        total += len(common)
        common = list(string.ascii_lowercase)

print(total)
