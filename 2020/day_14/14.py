import sys
import re

inputs = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

values = {}

def process_mask(line):
    mask = list(line.split(" = ")[1])
    mask.reverse()

    mask_and = 0
    mask_or = 0

    for i, b in enumerate(mask):
        mult = 2**i
        if b != '0':
            mask_and += mult

        if b == '1':
            mask_or += mult

    # print(f"Setting mask to {mask}")
    # print(f"Mask AND {mask_and}")
    # print(f"Mask OR {mask_or}")
    return mask_and, mask_or

def process_val(line, mask_and, mask_or):
    match = re.match("mem\[(\d+)\] = (\d+)", l)
    addr = match[1]
    v = int(match[2])
    masked = (v & mask_and) | mask_or
    # print(f"Value for address {addr} is {v} (Masked {masked})")
    return addr, masked

res = {}
mask_and = 0
mask_or = 0

for l in inputs:
    if re.match("mask", l):
        mask_and, mask_or = process_mask(l)
    else:
        addr, val = process_val(l, mask_and, mask_or)
        res[addr] = val

total = 0
for v in res.values():
    total += v

print(f"Part 1: {total}")

def bin_array(i, num_pos):
    return list(bin(i)[2:].zfill(num_pos))


memory = {}
curr_mask = 0
for line in inputs:
    k, v = line.split(" = ")
    if k == "mask":
        curr_mask = v
    else:
        # get address
        curr_address = int(k[4:-1])
        curr_value = int(v)

        bin_add = bin_array(curr_address, 36)
        new_add = ["0"] * 36

        for i, (mask, value) in enumerate(zip(curr_mask, bin_add)):
            # if mask bit is floating then keep it floating on new address too
            if mask == "X":
                new_add[i] = "X"
            elif mask == "0":

                # change value to mask else
                new_add[i] = value
            elif mask == "1":
                new_add[i] = "1"

        new_add = "".join(new_add)

        # count floatings
        num_poss = new_add.count("X")

        flucts = []
        for i in range(2**num_poss):
            flucts.append( bin_array(i, num_poss))

        for fluct in flucts:
            i = 0
            nadd = ""
            for a in new_add:
                if a == "X":
                    nadd+=str(fluct[i])
                    i+=1
                else:
                    nadd+=str(a)
            memory[int(nadd, 2)]=curr_value
res = sum(memory.values())
print(f"Part 2: {res}")
