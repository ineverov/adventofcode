import sys

passwords = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

def parse(s):
    rules, st = s.split(": ")
    rule, sym = rules.split(' ')
    min_l, max_l = rule.split('-')

    min_l = int(min_l)
    max_l = int(max_l)
    # print([min_l, max_l, sym, st])
    return [min_l, max_l, sym, st]

def valid_old(min_l, max_l, sym, st):
    count = st.count(sym)
    # print(sym, ', ', st, ': ',  count)
    return True if count >= min_l and count <= max_l else False

def valid_new(first, second, sym, st):
    s1 = st[first-1]
    s2 = st[second-1]
    if (s1 == sym or s2 == sym) and s1 != s2:
        # print(first, second, sym, st)
        return True
    else:
        return False


pass_rules = list(map(parse, passwords))

print(sum(valid_old(*rule) for rule in pass_rules))
print(sum(valid_new(*rule) for rule in pass_rules))

# print(pass_rules)
