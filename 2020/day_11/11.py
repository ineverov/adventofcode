import sys
import copy
import time

lines = list(filter(None, open(sys.argv[1], "r").read().split("\n")))
layout = [ list(l) for l in lines ]

tollerance = int(sys.argv[2]) if len(sys.argv) > 2 else 4
depth = True if len(sys.argv) > 3 else False

def inspect(layout):
    print("\033c")
    for l in layout:
        print("".join(l))

inspect(layout)
generations = []

def count_neightbors_with_depth(layout, i, j, deep=False):
    h = len(layout)
    w = len(layout[0])
    if deep:
        depth = max([h,w])
    else:
        depth = 1
    sq = max([h,w])
    counter = 0

    tl  = [[i-shift, j-shift] for shift in list(range(1, sq))[:depth] if i-shift >= 0 and j-shift >= 0]
    br  = [[i+shift, j+shift] for shift in list(range(1, sq))[:depth] if i+shift < h and j+shift < w]
    tr  = [[i-shift, j+shift] for shift in list(range(1, sq))[:depth] if i-shift >= 0 and j+shift < w]
    bl  = [[i+shift, j-shift] for shift in list(range(1, sq))[:depth] if i+shift < h and j-shift >= 0]
    l = [[i, j-shift] for shift in list(range(1, j+1))[:depth] if j-shift >= 0]
    r = [[i, j+shift] for shift in list(range(1, w-j))[:depth] if j-shift < w]
    t = [[i-shift, j] for shift in list(range(1, i+1))[:depth] if i-shift >= 0]
    b = [[i+shift, j] for shift in list(range(1, h-i))[:depth] if i+shift < h]

    for direction in [l, tl, t, tr, r, br, b, bl]:
        # print(f"Checking direction {direction}")
        for cell in direction:
            val = layout[cell[0]][cell[1]]
            if val == '#':
                counter += 1
                break
            elif val == 'L':
                break
            else:
                pass

    return counter

def decide(i, j, layout, new_layout, depth, tollerance):
    if layout[i][j] == '.':
        return

    counter = count_neightbors_with_depth(layout, i, j, deep=depth)

    # print(f"Cell ({i}, {j}) has {counter} neighbors")
    if counter >= tollerance:
        new_layout[i][j] = 'L'
    elif counter == 0 and layout[i][j] == 'L':
        new_layout[i][j] = '#'


def flatten(l):
    return [item for sublist in l for item in sublist]

gen = 0
if not generations or generations[-1] != layout:
    print('OK')
else:
    print('ERROR')
while not generations or generations[-1] != layout:
    gen += 1
    print(f"\nGeneration {gen}\n")

    new_layout = copy.deepcopy(layout)
    generations.append(layout)
    for i in range(len(layout)):
        for j in range(len(layout[0])):
            decide(i, j, layout, new_layout, depth, tollerance)
    inspect(new_layout)
    # time.sleep(1)
    layout = new_layout

print(flatten(generations[-1]).count('#'))
