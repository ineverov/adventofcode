import sys
import copy

code = [ int(n) for n in list(filter(None, open(sys.argv[1], "r").read().split("\n"))) ]
# print(code)

preamble = int(sys.argv[2]) if len(sys.argv) > 2 else 25
# print(preamble)

def valid(arr, number):
    for i, x in enumerate(arr):
        for j, y in enumerate(arr[i+1:]):
            if x + y == number:
                return True, i, i+j+1
    return False

def invalid():
    for i, num in enumerate(code[preamble:]):
        pre = code[i:preamble+i]
        if not valid(pre, num):
            return num

inval = invalid()
print(f"First invalid number is {inval}")

def contiguous_set():
    for i, x in enumerate(code):
        index = i
        total = x
        cont_set = [x]
        for y in code[i+1:]:
            cont_set.append(y)
            total += y
            # print(cont_set, total)
            if total == inval:
                return cont_set
            elif total > inval:
                break

cont_set = contiguous_set()
print(f"Encryption weakness is {min(cont_set) + max(cont_set)}")
