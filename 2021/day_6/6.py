import sys
import copy
import pdb

fishes = [ int(age) for age in open(sys.argv[1], "r").read().split(",")]
cp_fishes = copy.deepcopy(fishes)

def simulate(days, fishes):
    for i in range(days):
        size = len(fishes)

        for n, f in enumerate(fishes):
            if not fishes[n]:
                fishes[n] = 6
                fishes.append(9)
            else:
                fishes[n] -= 1
    return len(fishes)

print(f"Part 1: {simulate(80, fishes)}")

count_by_timer = {}

for i in range(9):
    count_by_timer[i] = 0

for f in cp_fishes:
    count_by_timer[f] += 1

for d in range(256):
    timers = count_by_timer.keys()

    count_by_timer_new = {}

    for i in range(9):
        count_by_timer_new[i] = 0

    for t in timers:
        if t:
            count_by_timer_new[t-1] = count_by_timer[t] + count_by_timer_new[t - 1]
        else:
            count_by_timer_new[8] = count_by_timer[0]
            count_by_timer_new[6] = count_by_timer[0] + count_by_timer_new[6]
    count_by_timer = count_by_timer_new

print(f"Part 2: {sum(count_by_timer_new.values())}")

