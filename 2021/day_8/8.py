import sys
import pdb

lines = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

def process_line(line):
    ten, code = line.split(' | ')
    zero_nine = [list(d) for d in ten.split(' ')]
    code_digits = [list(d) for d in code.split(' ')]
    for d in zero_nine:
        d.sort()
    for d in code_digits:
        d.sort()
    return zero_nine, code_digits

def sub(l1, l2):
    return [item for item in l1 if item not in l2]

def inter(l1, l2):
    return list(set(l1).intersection(l2))

def part1(lines):
    count = sum([ 1 for l in lines for c in process_line(l)[1] if len(c) in [2, 3, 4, 7]])
    print(f'Part 1: {count}')

def part2(lines):
    numbers = []

    for l in lines:
        digits, code = process_line(l)
        final = [None for i in range(10)]

        used_letters = []
        srtd = sorted(digits, key=len)

        # remaining 0-9
        final[1], final[7], final[4], final[8] = [ d for d in srtd if len(d) in [2,3,4,7] ]

        # remaining 0, 2, 3, 5, 6, 9
        remaining = sub(srtd, final)

        for d in remaining:
            if len(inter(final[4], d)) == 4:
                final[9] = d
                break

        remaining = sub(remaining, [final[9]])

        # remaining 0, 2, 3, 5, 6
        # six dig: 0, 6;  5 dig: 2, 3, 5

        six_digits = [d for d in remaining if len(d) == 6]
        five_digits = sub(remaining, six_digits)

        if len(inter(six_digits[0], final[7])) == 3:
            final[0] = six_digits[0]
            final[6] = six_digits[1]
        else:
            final[0] = six_digits[1]
            final[6] = six_digits[0]

        remaining = sub(remaining,  six_digits)

        # remaining 2, 3, 5

        for d in five_digits:
            if len(inter(final[6], d)) == 5:
                final[5] = d
                five_digits = sub(five_digits,  [d] )
                break

        # remaining 2, 3

        if len(sub(final[1],five_digits[0])) == 1:
            final[2] = five_digits[0]
            final[3] = five_digits[1]
        else:
            final[2] = five_digits[1]
            final[3] = five_digits[0]

        # Found all digits
        res = 0
        for cd in code:
            res *= 10
            res += final.index(cd)
        numbers.append(res)
    print(f'Part 2: {sum(numbers)}')


part1(lines)
part2(lines)

