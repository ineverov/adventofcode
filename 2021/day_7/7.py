import sys
import statistics

positions = [ int(pos) for pos in open(sys.argv[1], "r").read().split(",")]

mn, mx, md = min(positions), max(positions), int(statistics.median(positions))

fuel = sum([ abs(md - p) for p in positions])
print(f"Part 1: {fuel}")

gradient = 0

def fuel_needed(start, end):
    distance = abs(start - end)
    return distance * (distance + 1) // 2

def total_fuel(positions, point):
    s = 0
    for p in positions:
        s += fuel_needed(p, point)
    return s

current_pos = md
grad = 0
less, cur, n = 0, 0, 0
while True:
    less, cur, n = total_fuel(positions, current_pos - 1), total_fuel(positions, current_pos), total_fuel(positions, current_pos + 1)

    if cur < less and cur < n:
        break
    elif cur > less:
        current_pos -= 1
    else:
        current_pos += 1

print(f"Part 2: {int(cur)}")

