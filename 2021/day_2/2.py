import sys

records = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

x, y = 0, 0

for command in records:
    cmd, arg = command.split(" ")
    arg = int(arg)

    if cmd == 'forward':
        x += arg
    elif cmd == 'down':
        y += arg
    else:
        y -= arg

print(f"Part 1: x={x} y={y} result={x*y}")

x, y, aim = 0, 0, 0

for command in records:
    cmd, arg = command.split(" ")
    arg = int(arg)

    if cmd == 'forward':
        x += arg
        y += aim * arg
    elif cmd == 'down':
        aim += arg
    else:
        aim -= arg

print(f"Part 2: aim={aim} x={x} y={y} result={x*y}")
