import sys

lines = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

hm = [ [int(c)  for c in list(l) ] for l in lines ]

m,n = len(hm), len(hm[0])

lowest_points = []

def adjust(y, x):
    adj = [[y-1, x], [y+1, x], [y, x-1], [y, x+1]]
    return [ p for p in adj if p[0] >= 0 and p[1] >= 0 and p[0] < m and p[1] < n ]

for y, l in enumerate(hm):
    for x, c in enumerate(l):
        less = True

        for p in adjust(y, x):
            if c >= hm[p[0]][p[1]]:
                less = False
                break
        if less:
            lowest_points.append([y, x, c])

print(f"Part 1: {sum([ p[2] + 1 for p in lowest_points ])}")

def basin(start, basin_points, hm):
    adj = adjust(start[0], start[1])

    if start[2] == 8:
        return basin
    for a in adj:
        adj_val = hm[a[0]][a[1]]
        if adj_val > start[2] and adj_val < 9 and [a[0], a[1]] not in basin_points:
            basin_points.append([a[0], a[1]])
            basin([a[0], a[1], adj_val], basin_points, hm)

basins = []
for l in lowest_points:
    curr_basin = [l[0:-1]]
    basin(l, curr_basin, hm)
    basins.append(curr_basin)

sizes = [len(b) for b in basins]
sizes.sort(reverse=True)
results = sizes[0:3]
res = 1
for r in results:
    res *= r

# print(f"Results {results}")

print(f"Part 2: {res}")
