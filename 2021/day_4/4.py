import sys
import re
import numpy as np
import copy

inputs = open(sys.argv[1], "r").read().split("\n")

numbers = list(inputs[0].split(','))

boards = []

current_board = []

for line in inputs[2:] :
    if not line:
        boards.append([np.array(current_board).tolist(), np.array(current_board).transpose().tolist()])
        current_board = []
        continue


    l = list(filter(None, list(re.sub(r'\s{1,}', ' ', line).split(' '))))
    current_board.append(l)

# Copy for part 2
boards_copy = copy.deepcopy(boards)

result = None
for n in numbers:
    for bs in boards:
        for b in bs:
            for l in b:
                if n in l:
                    l.remove(n)
                if not list(l):
                    result = b
                    last_digit = n
                    break
            if result:
                break
        if result:
            break
    if result:
        break

flat_list = [item for sublist in result for item in sublist]

total = 0
for i in flat_list:
    total += int(i)

print(f"Part 1: { total * int(last_digit)}")


total = len(boards)
won = 0
last_won = None

won_boards = []
result = None
for n in numbers:
    for bn, bs in enumerate(boards_copy):
        if bn in won_boards:
            continue
        board_won = False
        for b in bs:
            for l in b:
                if n in l:
                    l.remove(n)

                if not l:
                    board_won = True
                    last_won = b
                    last_digit = n
                    won_boards.append(bn)
                    break
            if board_won:
                break
        if board_won:
            won += 1
            if won == total:
                result = last_won
                break
    if result:
        break


flat_list = [item for sublist in result for item in sublist]

total = 0
for i in flat_list:
    total += int(i)

print(f"Part 2: { total * int(last_digit)}")
