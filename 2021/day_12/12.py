import sys
import pdb
import copy

lines = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

directions = {}

routes = []

for l in lines:
    fr, to = l.split('-')

    if fr not in directions:
        directions[fr] = [to]
    else:
        directions[fr].append(to)

    if to not in directions:
        directions[to] = [fr]
    else:
        directions[to].append(fr)

def large(cave):
    if cave.lower() == cave:
        return False
    else:
        return True

def can_go(cave, visited):
    return large(cave) or cave not in visited

def travel(starting, directions, visited, routes):
    if starting not in directions:
        return None

    options = directions[starting]

    filtered_options = []

    if starting == 'end':
        routes.append(visited)
        return routes

    for o in options:
        if can_go(o, visited):
            travel(o, directions, visited + [o], routes)

# print(directions)
travel('start', directions, ['start'], routes)

# for i, r in enumerate(routes):
#     print(f"Route {i+1}: {','.join(r)}")

print(f"Part 1: {len(routes)}")

routes = []

def out(cave):
    return cave == 'start' or cave == 'end'

def small(cave):
    return not large(cave) and not out(cave)

def visited_twice(visited):
    visits = {}
    for v in visited:
        if large(v):
            continue
        if v in visits:
            visits[v] += 1
        else:
            visits[v] = 1

    if 2 in visits.values():
        return True
    else:
        return False

def can_go(cave, visited):
    # pdb.set_trace()
    visited_time = visited.count(cave)
    return large(cave) or out(cave) and visited_time == 0 or small(cave) and (visited_time == 0 or not visited_twice(visited))

travel('start', directions, ['start'], routes)

print(f"Part 2: {len(routes)}")
