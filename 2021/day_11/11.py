import sys
import pdb
import copy

lines = list(filter(None, open(sys.argv[1], "r").read().split("\n")))
octopuses = [ [int(o) for o in list(l) ] for l in lines ]

original = copy.deepcopy(octopuses)

m, n = len(octopuses), len(octopuses[0])

def adjust(y, x):
    adj = [[y-1, x], [y-1, x+1],  [y, x+1], [y+1, x+1], [y+1, x], [y+1, x-1], [y, x-1], [y-1, x-1]]
    return [ p for p in adj if p[0] >= 0 and p[1] >= 0 and p[0] < m and p[1] < n ]


def iterate_until_flashed(octopuses, flashed_octos):
    new_octopuses = copy.deepcopy(octopuses)
    for y, l in enumerate(octopuses):
        for x, c in enumerate(l):
            if new_octopuses[y][x] > 9 and [y, x] not in flashed_octos:
                # print(f"Flashed: ({y}, {x})")
                flashed = True
                new_octopuses[y][x] = 0
                flashed_octos.append([y, x])
                for p in adjust(y, x):
                    if [p[0], p[1]] not in flashed_octos:
                        new_octopuses[p[0]][p[1]] += 1
                return new_octopuses, True
    return new_octopuses, False


def do_step(octopuses):
    flashed = True
    counter = 0
    flashed_octos = []
    for y, l in enumerate(octopuses):
        for x, o in enumerate(l):
            octopuses[y][x] += 1
    while flashed:
        octopuses, flashed = iterate_until_flashed(octopuses, flashed_octos)
        if flashed:
            counter += 1
    print_octo(octopuses)
    return counter, flashed_octos, octopuses

def print_octo(octopuses):
    for l in octopuses:
        print(l)

total = 0
for i in range(100):
    counter, flashed, octopuses = do_step(octopuses)
    total += counter
    print(f"After step: {i}. Flashed: {counter}. Points: {flashed}")

print(f"Part 1: {total}")


def is_sync(octopuses):
    if sum([ v for l in octopuses for v in l]) == 0:
           return True
    else:
           return False

octopuses = original

total = 0
step = 0
sync = False
while not sync:
    counter, flashed, octopuses = do_step(octopuses)
    sync = is_sync(octopuses)
    total += counter
    step += 1
    print(f"After step: {i}. Flashed: {counter}. Points: {flashed}")
print(f"Part 2: {step}")
