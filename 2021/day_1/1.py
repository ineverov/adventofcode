import sys

depths = list(map(lambda x: int(x), list(filter(None, open(sys.argv[1], "r").read().split("\n")))))

count = 0
for ind in range(len(depths)-1):
    if depths[ind] < depths[ind+1]:
        count += 1



print(f"Part 1: {count}")

count = 0
for ind in range(len(depths)-3):
    if depths[ind] + depths[ind+1] + depths[ind+2] < depths[ind+1] + depths[ind + 2] + depths[ind + 3]:
        count += 1

print(f"Part 2: {count}")
