File.read(ARGV[0])
  .split
  .map { |l| l.chars.map(&:to_i).map { |b| b == 0 ? -1 : 1 } }
  .transpose
  .reverse
  .each_with_index
  .map { |bits, i| bits.inject(&:+).positive? ? [2**i, 0] : [0, 2**i] }
  .transpose
  .map { |param| param.inject(&:+) }
  .inject(&:*)
