import sys

binaries = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

freq=[]

for i in range(len(binaries[0])):
    freq.append({ "0": 0, "1": 0 })


for b in binaries:
    for i, bit in enumerate(list(b)):
        freq[i][bit] += 1

gamma = ""
epsilon = ""

for bit in freq:
    if bit['0'] > bit['1']:
        gamma += '0'
        epsilon += '1'
    else:
        gamma += '1'
        epsilon += '0'



g = int(gamma, 2)
e = int(epsilon, 2)
print(f"Part 1: {g*e}")


filter_bit = ""

current_list = binaries
i = -1

while not len(current_list) == 1:
    i += 1
    new_list = []

    zeros = 0
    ones = 0

    for b in current_list:
        if b[i] == '0':
            zeros += 1
        else:
            ones += 1

    if zeros == ones:
        filter_bit = '1'
    elif zeros > ones:
        filter_bit = '0'
    else:
        filter_bit = '1'

    for b in current_list:
        if b[i] == filter_bit:
            new_list.append(b)

    current_list = new_list

oxy = int(current_list[0], 2)

filter_bit = ""

current_list = binaries
i = -1

while not len(current_list) == 1:
    i += 1
    new_list = []

    zeros = 0
    ones = 0

    for b in current_list:
        if b[i] == '0':
            zeros += 1
        else:
            ones += 1

    if zeros == ones:
        filter_bit = '0'
    elif zeros > ones:
        filter_bit = '1'
    else:
        filter_bit = '0'

    for b in current_list:
        if b[i] == filter_bit:
            new_list.append(b)

    current_list = new_list

co2 = int(current_list[0], 2)
print(f"Part 2: {oxy * co2}")
