import sys
import pdb
import copy
from collections import defaultdict

lines = open(sys.argv[1], "r").read().split("\n")[0:-1]

initial_str = lines[0]

rule_lines = lines[2:]

first = initial_str[0]
last = initial_str[-1]

# print(f"Initial str: {initial_str}")
# print(f"Rules: {rule_lines}")
def parse_rules(rule_lines):
    rules = defaultdict(list)
    for l in rule_lines:
        k, v = l.split(' -> ')
        rules[k] = [k[0]+v, v+k[1]]
    return rules

def initial_pairs(initial):
    pairs = defaultdict(int)
    for i in range(len(initial)-1):
        pairs[initial[i:i+2]] += 1
    return pairs

rules = parse_rules(rule_lines)
initial = initial_pairs(initial_str)



def make_step(pairs, rules):
    new_pairs = copy.deepcopy(pairs)
    for p in pairs:
        if pairs[p]:
            new_pairs[p] -= pairs[p]
            for r in rules[p]:
                new_pairs[r] += pairs[p]
    return new_pairs

def freqs(pairs, first, last):
    freq = defaultdict(int)
    for p in pairs:
        if pairs[p]:
            for s in list(p):
                freq[s] += pairs[p]
    for f in freq:
        freq[f] = freq[f] // 2

    freq[first] += 1
    freq[last] += 1
    return freq

def max_min(freq):
    mn, mx = float('inf'), 0

    for k in freq:
        if freq[k] > mx:
            mx = freq[k]
        if freq[k] < mn:
            mn = freq[k]
    return mn, mx

new=initial
mx, mn = 0, 0
for i in range(10):
    # print(f"Step {i}:")
    new = make_step(new, rules)
    # print(new)
    freq = freqs(new, first, last)
    # print(f"Freqs: {freq}")
    # print(f"Length: {sum(freq.values())}")
    mn, mx = max_min(freq)
    # print(f"Max {mx}, Min: {mn}, Result: {mx - mn}")

print(f"Part 1: {mx - mn}")

for i in range(30):
    # print(f"Step {i}:")
    new = make_step(new, rules)
    # print(new)
    freq = freqs(new, first, last)
    # print(f"Freqs: {freq}")
    # print(f"Length: {sum(freq.values())}")
    mn, mx = max_min(freq)
    # print(f"Max {mx}, Min: {mn}, Result: {mx - mn}")

print(f"Part 2: {mx - mn}")
