import sys

lines = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

lines = [ list(l) for l in lines ]

corrupted_points = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137
}

complete_points = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4
}

right = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>'
}

def illegal_char(l):
    stack = []
    for s in l:
        if s in right.keys():
            stack.append(s)
        else:
            top = stack.pop(-1)
            if right[top] != s:
                return 'corrupted', s
    if stack:
        return 'complete', stack
    return None, None


def score_for_line(l, score_type = 'corrupted'):
    status, chars = illegal_char(l)
    if status =='corrupted' and score_type == 'corrupted':
        return corrupted_points[chars]
    elif status == 'complete' and score_type == 'complete':
        total = 0
        chars.reverse()
        for c in chars:
            total *= 5
            total += complete_points[right[c]]
        return total
    else:
        return 0

total = sum([ score_for_line(l) for l in lines ])
print(f"Part 1: {total}")

line_points = [ score_for_line(l, 'complete') for l in lines ]
line_points = [ p for p in line_points if p > 0 ]
line_points.sort()
res = line_points[len(line_points) // 2 ]

print(f"Part 2: {res}")
