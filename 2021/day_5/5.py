import sys
import copy
import pdb

inputs = list(filter(None, open(sys.argv[1], "r").read().split("\n")))

def segments(inputs):
    return [[[int(c) for c in p_str.split(',')] for p_str in l.split(' -> ')] for l in inputs ]

segments = segments(inputs)

def add_point(field, x, y, prnt=False):
    if prnt:
        print(f"Adding point ({x}, {y})")
    field[y][x] += 1

def solution(segments, diag = True, prnt=False):
    max_x = max([p[0] for segment in segments for p in segment] )
    max_y = max([p[0] for segment in segments for p in segment] )

    field = [[ 0 for x in range(max_x+1) ] for y in range(max_y+1) ]
    for seg in segments:
        if prnt:
            print(f"For segment {seg}")
        size_x = seg[0][1] - seg[1][1]
        size_y = seg[0][0] - seg[1][0]

        dir_x = 1 if size_x < 0 else -1
        dir_y = 1 if size_y < 0 else -1
        size = max(abs(size_x), abs(size_y))

        if size_x == 0:
            for i in range(size + 1):
                x, y = seg[0][0] + dir_y * i, seg[0][1]
                add_point(field, x, y, prnt)
        elif size_y == 0:
            for i in range(size + 1):
                x, y = seg[0][0], seg[0][1] + dir_x * i
                add_point(field, x, y, prnt)
        elif diag:
            for i in range(size + 1):
                x, y = seg[0][0] + dir_y * i, seg[0][1] + dir_x * i
                add_point(field, x, y, prnt)
    if prnt:
        for l in field:
            print(l)
    return sum([ 1 for r in field for c in r if c > 1 ])


s1 = solution(segments, False)
print(f"Part 1: {s1}")
s2 = solution(segments, True)
print(f"Part 2: {s2}")
