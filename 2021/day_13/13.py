import sys
import pdb
import copy

lines = open(sys.argv[1], "r").read().split("\n")[0:-1]
dots = []
folds = []

def parse_dot(l):
    return([ int(c) for c in l.split(',') ])

def parse_fold(l):
    fold = l.split(' ')[2].split('=')
    return([fold[0], int(fold[1])])

def print_paper(paper):
    for l in paper:
        print(''.join([ '#' if i == 1 else '.' for i in l ]))


def list_to_str(l):
    return ''.join([str(e) for e in l])

def fold_left(paper, coord):
    new_paper = []
    for l in paper:
        left = l[:coord]
        right = l[coord+1:]
        left_n = int(list_to_str(left), 2)
        right_n = int(list_to_str(right[::-1]), 2)
        result = left_n | right_n
        new_l = list(format(result, f'0{len(left)}b'))
        new_paper.append([ int(e) for e in new_l ])
    return new_paper

def fold_up(paper, coord):
    new_paper = [list(x) for x in zip(*paper)]
    folded =  fold_left(new_paper, coord)
    return  [list(x) for x in zip(*folded)]

current_type = 'dots'
for l in lines:

    if l:
        if current_type == 'dots':
            dots.append(parse_dot(l))
        else:
            folds.append(parse_fold(l))
    else:
        current_type = 'folds'

max_x = max([p[0] for p in dots])
max_y = max([p[1] for p in dots])

paper = [[ 0 for x in range(max_x+1)] for y in range(max_y+1)]

for dot in dots:
    paper[dot[1]][dot[0]] = 1


for fold in folds[0:1]:
    if fold[0] == 'y':
        paper = fold_up(paper, fold[1])
    else:
        paper = fold_left(paper, fold[1])

total = sum([1 for line in paper for p in line if p == 1])

print(f"Part 1: {total}")

for fold in folds[1:]:
    if fold[0] == 'y':
        paper = fold_up(paper, fold[1])
    else:
        paper = fold_left(paper, fold[1])

print(f"Part 2")
print_paper(paper)
