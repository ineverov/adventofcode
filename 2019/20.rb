# input = <<-START
# .#..##.###...#######
# ##.############..##.
# .#.######.########.#
# .###.#######.####.#.
# #####.##.#.##.###.##
# ..#####..#.#########
# ####################
# #.####....###.#.#.##
# ##.#################
# #####.##.###..####..
# ..######..##.#######
# ####.##.####...##..#
# .#####..#.######.###
# ##...#.##########...
# #.##########.#######
# .####.#.###.###.#.##
# ....##.##.###..#####
# .#.#.###########.###
# #.#.#.#####.####.###
# ###.##.####.##.#..##
# START

# input = <<-START
# .#....#####...#..
# ##...##.#####..##
# ##...#...#.#####.
# ..#.....#...###..
# ..#.#.....#....##
# START

input = <<-START
#.....#...#.........###.#........#..
....#......###..#.#.###....#......##
......#..###.......#.#.#.#..#.......
......#......#.#....#.##....##.#.#.#
...###.#.#.......#..#...............
....##...#..#....##....#...#.#......
..##...#.###.....##....#.#..##.##...
..##....#.#......#.#...#.#...#.#....
.#.##..##......##..#...#.....##...##
.......##.....#.....##..#..#..#.....
..#..#...#......#..##...#.#...#...##
......##.##.#.#.###....#.#..#......#
#..#.#...#.....#...#...####.#..#...#
...##...##.#..#.....####.#....##....
.#....###.#...#....#..#......#......
.##.#.#...#....##......#.....##...##
.....#....###...#.....#....#........
...#...#....##..#.#......#.#.#......
.#..###............#.#..#...####.##.
.#.###..#.....#......#..###....##..#
#......#.#.#.#.#.#...#.#.#....##....
.#.....#.....#...##.#......#.#...#..
...##..###.........##.........#.....
..#.#..#.#...#.....#.....#...###.#..
.#..........#.......#....#..........
...##..#..#...#..#...#......####....
.#..#...##.##..##..###......#.......
.##.....#.......#..#...#..#.......#.
#.#.#..#..##..#..............#....##
..#....##......##.....#...#...##....
.##..##..#.#..#.................####
##.......#..#.#..##..#...#..........
#..##...#.##.#.#.........#..#..#....
.....#...#...#.#......#....#........
....#......###.#..#......##.....#..#
#..#...##.........#.....##.....#....
START

require 'byebug'

points = []
lines = input.split
lines.each_with_index do |l, y|
  l.chars.each_with_index do |s, x|
    points << [x, y] if s == '#'
  end
end

best = points.first

map = {}

points.each do |p1|
  map[p1] = {}
  other = points - [p1]

  other.each do |p2|
    ay = p1.last - p2.last
    ax = p1.first - p2.first

    k = Rational(ay, ax) if ax != 0

    el = [k, ax <= 0, ay <= 0]

    map[p1][el] ||= []
    map[p1][el] << p2
  end
end


point, descs = map.max { |(k1, v1), (k2, v2)| v1.count <=> v2.count }
puts point.inspect
puts descs.count

counter = 0
result = nil

coords = map[point]

def val(data)
  return data[0] if data[0]

  return data[2] ? Float::INFINITY : -Float::INFINITY
end

candidates = []

quat1 = coords.select { |data, asts| data[1] && !data[2] }
candidates += quat1.sort { |(data_a, asts_a), (data_b, asts_b)| val(data_a) <=> val(data_b) }

quat2 = coords.select { |data, asts| data[1] && data[2] }
candidates += quat2.sort { |(data_a, asts_a), (data_b, asts_b)| val(data_a) <=> val(data_b) }

quat3 = coords.select { |data, asts| !data[1] && data[2] }
candidates += quat3.sort { |(data_a, asts_a), (data_b, asts_b)| val(data_a) <=> val(data_b) }

quat4 = coords.select { |data, asts| !data[1] && !data[2] }
candidates += quat4.sort { |(data_a, asts_a), (data_b, asts_b)| val(data_a) <=> val(data_b) }

def distance(a,b)
  (a[0] - b[0])**2 + (a[1] - b[1])**2
end

candidates.each do |c|
  c[1].sort! do |a1, a2|
     distance(point, a1) <=> distance(point, a2)
  end
end

ex = false

res = nil

loop do
  candidates.each do |cand|
    res = cand[1].shift
    if res
      counter += 1
      puts "Counter: #{counter}. #{res}"
    end
    if counter == 200
      ex = true
      break
    end
  end
  break if ex
end

puts res[0]*100+res[1]
