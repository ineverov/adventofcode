input = <<-STR
<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>
STR

input = <<-STR
<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>
STR

input = <<-STR
<x=-6, y=-5, z=-8>
<x=0, y=-3, z=-13>
<x=-15, y=10, z=-11>
<x=-3, y=-8, z=3>
STR

lines = input.split("\n")
sats = lines.map do |coord_str|
  coord_str.split(', ').map { |c| c.split('=').last.to_i }
end

class Sat
  attr_reader :coords, :vel

  def initialize(coords)
    @coords = coords
    @vel = [0, 0, 0]
  end

  def apply_gravity(sat)
    shift = [coords, sat.coords].transpose.map do |pair|
      if pair[0] < pair[1]
        [1, -1]
      elsif pair[1] < pair[0]
        [-1, 1]
      else
        [0, 0]
      end
    end

    adjust = shift.transpose
    add_velocity(adjust.first)
    sat.add_velocity(adjust.last)
  end

  def add_velocity(vector)
    3.times do |i|
      @vel[i] += vector[i]
    end
  end

  def apply_velocity
    3.times do |i|
      @coords[i] += vel[i]
    end
  end

  def to_s
    "POS: <x:\t#{coords[0]}, y:\t#{coords[1]},\tz: #{coords[2]}>. VEL <x:\t#{vel[0]}, y:\t#{vel[1]},\tz: #{vel[2]}>. E: #{energy}"
  end

  def energy
    potential * kinetic
  end

  def potential
    coords.map(&:abs).inject(&:+)
  end

  def kinetic
    vel.map(&:abs).inject(&:+)
  end
end

steps = 1000

sats = sats.map { |s| Sat.new(s) }

def interact(sats)
  sats[1..-1].each do |other|
    sats[0].apply_gravity(other)
  end

  interact(sats[1..-1]) unless sats[1..-1].empty?
end

puts "Step 0"
sats.each do |s|
  puts s.to_s
end

steps.times do |i|
  puts "Step #{i + 1}"

  interact(sats)

  sats.each do |s|
    s.apply_velocity
    puts s.to_s
  end

  puts "Total energy: #{sats.map(&:energy).inject(&:+)}"
end
