require 'byebug'
require 'ruby2d'

input = File.read('5.txt')
wires = input.split
wire1 = wires.first.split(',')
wire2 = wires.last.split(',')

def process(wire)
  points = []

  point = [0, 0]

  points << point.clone

  wire.each do |directions|
    distance = directions[1..-1].to_i
    case directions[0]
    when 'U'
      point[1] += distance
    when 'D'
      point[1] -= distance
    when 'L'
      point[0] -= distance
    when 'R'
      point[0] += distance
    end
    points << point.clone
  end
  points
end

points1 = process(wire1)
points2 = process(wire2)

# points1.each do |point|
#   puts "x: #{point[0]}, y: #{point[1]}"
# end
# puts
# points2.each do |point|
#   puts "x: #{point[0]}, y: #{point[1]}"
# end
# puts



class MyLine
  attr_reader :vertical, :minmax, :const, :point, :start

  def initialize(p1, p2, point = false)
    @point = point
    @vertical = if p1[0] == p2[0]
                  true
                else
                  false
                end

    @start = p1
    if @vertical
      @const = p1[0]
      @minmax = [p1[1], p2[1]].sort
    else
      @const = p1[1]
      @minmax = [p1[0], p2[0]].sort
    end
  end

  def to_s
    "MyLine(#{vertical ? 'vertical' : 'horizontal'}, #{const}, #{minmax})"
  end

  def intersect(other)
    if vertical ^ other.vertical || other.point
      if other.point
        if vertical && other.start.last >= minmax.first && other.start.last <= minmax.last && const == other.start.first
          other.start
        elsif !vertical && other.start.first >= minmax.first && other.start.first <= minmax.last && const == other.start.last
          other.start
        else
          nil
        end
      elsif const >= other.minmax.first && const <= other.minmax.last && other.const >= minmax.first && other.const <= minmax.last
        vertical ? [[const, other.const]] : [[other.const, const]]
      else
        nil
      end
#    elsif const == other.const
#      common = [[minmax.first, minmax.first].max, [minmax.last, minmax.last].min]
#      if vertical
#        (common.first..common.last).map do |y|
#          [const, y]
#        end
#      else
#        (common.first..common.last).map do |x|
#          [x, const]
#        end
#      end
    else
      nil
    end
  end
end

def get_lines(points)
  (points.size - 1).times.to_a.map do |i|
    Line.new(
      x1: (points[i][0] + 10000)/30, y1: (points[i][1] + 2000)/30,
      x2: (points[i+1][0] + 10000)/30, y2: (points[i+1][1] + 2000)/30,
      width: 1,
      color: 'lime',
      z: 1
    )
    MyLine.new(points[i], points[i+1])
  end
end

lines1 = get_lines(points1)
lines2 = get_lines(points2)

intersections = []

lines1.each do |l1|
  lines2.each do |l2|
    if result = l1.intersect(l2)
      intersections += result
    end
  end
end

intersections.each do |point|
  puts "x: #{point[0]}, y: #{point[1]}"
end


min = nil
intersections[1..-1].each do |p|
  dist = p[0].abs + p[1].abs
  min = dist unless min
  if dist < min
    min = dist
  end
end

puts min


min_steps = 10000000000000

def steps(lines, iline)
  lines.inject(0) do |sum, line|
    if line.intersect(iline)
      if line.vertical
        sum += (line.start.last - iline.start.last).abs
      else
        sum += (line.start.first - iline.start.first).abs
      end
  #    puts sum
      return sum
    else
      sum += line.minmax.last - line.minmax.first
    end
  #  puts sum
    sum
  end
end

intersections[1..-1].each do |i|
  iline = MyLine.new(i, i, true)
  steps1 = steps(lines1, iline)
  steps2 = steps(lines2, iline)

  if min_steps > steps1 + steps2
    min_steps = steps1 + steps2
  end
end

puts "Min steps"
puts min_steps


# set fullscreen: true

# show
