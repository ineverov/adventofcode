image = File.read('15.txt')

require 'byebug'

width = 25
tall = 6

layers = image.strip.chars.map(&:to_i).each_slice(width * tall).to_a

layers_info = []

layers.each_with_index do |l, i|
  puts l.size
  count = [0, 0, 0]
  l.each do |d|
    count[d] += 1
  end
  layers_info[i] = count
end

min_layer = layers_info.min_by { |i| i[0] }
puts min_layer[1] * min_layer[2]
