require 'byebug'
input = [3,8,1001,8,10,8,105,1,0,0,21,46,59,72,93,110,191,272,353,434,99999,3,9,101,4,9,9,1002,9,3,9,1001,9,5,9,102,2,9,9,1001,9,5,9,4,9,99,3,9,1002,9,5,9,1001,9,5,9,4,9,99,3,9,101,4,9,9,1002,9,4,9,4,9,99,3,9,102,3,9,9,101,3,9,9,1002,9,2,9,1001,9,5,9,4,9,99,3,9,1001,9,2,9,102,4,9,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,99,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,99]

class Intcode
  attr_reader :program, :inputs
  attr_accessor :position

  def initialize(program)
    @program = program
    @inputs = []
    @position = 0
    @halt = false
    @outputs = []
  end

  def add_input(inp)
    @inputs << inp
  end

  def halt?
    @halt
  end

  def output
    @outputs.last
  end

  def run!
    loop do
      cmd, mode1, mode2, mode3 = commands(program[position])

      case cmd
      when 1 # add
        new_value(
          position + 3,
          mode3,
          value(position + 1, mode1) + value(position + 2, mode2)
        )
        @position += 4
      when 2 # multiply
        new_value(
          position + 3,
          mode3,
          value(position + 1, mode1) * value(position + 2, mode2)
        )
        @position += 4
      when 3 # write
        new_value(
          position + 1,
          mode1,
          inputs.shift
        )
        @position += 2
      when 4 # output
        @outputs << value(position + 1, 0)
        @position += 2
        return output
      when 5 # jump-if-true
        if value(position + 1, mode1) != 0
          @position = value(position + 2, mode2)
        else
          @position += 3
        end
      when 6 # jump-if-false
        if value(position + 1, mode1).zero?
          @position = value(position + 2, mode2)
        else
          @position += 3
        end
      when 7 # less-than
        if value(position + 1, mode1) < value(position + 2, mode2)
          new_value(position + 3, mode3, 1)
        else
          new_value(position + 3, mode3, 0)
        end
        @position += 4
      when 8 # equals
        if value(position + 1, mode1) == value(position + 2, mode2)
          new_value(position + 3, mode3, 1)
        else
          new_value(position + 3, mode3, 0)
        end
        @position += 4
      when 99
        @position += 1
        @halt = true
        return output
      else
        raise "Error at #{position}"
      end
    end
  end

  def value(pos, mode)
    case mode
    when 1
      program[pos]
    when 0
      program[program[pos]]
    end
  end

  def new_value(pos, mode, value)
    case mode
    when 1
      program[pos] = value
    when 0
      program[program[pos]] = value
    end
  end

  def commands(value)
    command = value % 100
    mode1 = (value / 100) % 10
    mode2 = (value / 1000) % 10
    mode3 = (value / 10_000) % 10

    [command, mode1, mode2, mode3]
  end
end

max = 0
max_phases = [0, 0, 0, 0]

(5..9).each do |a|
  ((5..9).to_a - [a]).each do |b|
    ((5..9).to_a - [a, b]).each do |c|
      ((5..9).to_a - [a,b,c]).each do |d|
        ((5..9).to_a - [a, b, c, d]).each do |e|
          a_prog = Intcode.new(input.clone)
          b_prog = Intcode.new(input.clone)
          c_prog = Intcode.new(input.clone)
          d_prog = Intcode.new(input.clone)
          e_prog = Intcode.new(input.clone)

          result = 0
          first = true

          loop do
            a_prog.add_input(a) if first
            a_prog.add_input(result)
            result = a_prog.run!

            b_prog.add_input(b) if first
            b_prog.add_input(result)
            result = b_prog.run!

            c_prog.add_input(c) if first
            c_prog.add_input(result)
            result = c_prog.run!

            d_prog.add_input(d) if first
            d_prog.add_input(result)
            result = d_prog.run!

            e_prog.add_input(e) if first
            e_prog.add_input(result)
            result = e_prog.run!

            first = false
            break if e_prog.halt?
          end

          if result > max
            max = result
            max_phases = [a, b, c, d, e]
          end
        end
      end
    end
  end
end

puts
puts 'Result'

puts max
puts max_phases.inspect
