inputs = File.read('1.txt').split.map(&:to_i)

# inputs = [100756]

$hash = {}
$reuse = 0
$new = 0

def fuel(input)
  if $hash[input]
    $reuse += 1
    return $hash[input]
  else
    $new += 1
  end

  out = input / 3 - 2
  if out <= 0
    $hash[input] = 0
  else
    $hash[input] = out + fuel(out)
    $hash[input]
  end
end


puts inputs.inject(0) { |sum, input| sum + fuel(input) }
puts $new
puts $reuse
