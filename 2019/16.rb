image = File.read('15.txt')


require 'colorize'

width = 25
tall = 6

layers = image.strip.chars.map(&:to_i).each_slice(width * tall).to_a

layers_info = []


(0..149).map do |i|
  if i % 25 == 0
    puts
  end
  color = layers.map{|l| l[i]}.find { |c| c != 2 }
  if color == 1
    print '*'
  else
    print ' '
  end
end
