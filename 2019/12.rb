
input = <<-TEXT.split
COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)
K)YOU
I)SAN
TEXT

input = File.read('11.txt').split

$satelits = {}

input.each do |orb|
  left, right = orb.split(')')
  $satelits[left] ||= []
  $satelits[left] << right
end

def count(node, total)
  ($satelits[node] || []).inject(total) do |sum, sub|
    sum + count(sub, total + 1)
  end
end

require 'byebug'

def path_to_node(path, node, target)
  if ($satelits[node] || []).include?(target)
    path.unshift node
    return true
  end

  ($satelits[node] || []).each do |sub|
    if path_to_node(path, sub, target)
      path.unshift node
      return true
    end
  end

  return false
end
path_you = []
path_san = []

path_to_node(path_you, 'COM', 'YOU')
path_to_node(path_san, 'COM', 'SAN')
puts path_you.inspect
puts path_san.inspect


puts (path_you + path_san - (path_you & path_san)).size
