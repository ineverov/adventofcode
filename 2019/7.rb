require 'byebug'
start, $finish = 248345, 746315

puts start
puts $finish

$counter = 0
$start = true

def split(int)
  int.to_s.chars.map(&:to_i)
end

def checker(b_digs:, e_digs:, double_met:, prev_dig:, lvl: 0, finish: false)
  $start = false if $start && b_digs[lvl] < prev_dig

  range = if finish
            ([b_digs[lvl], prev_dig].max..e_digs[lvl])
          else
            ([b_digs[lvl], prev_dig].max..9)
          end

  if lvl == 5
    if double_met
      range.each do |d|
        num = b_digs.tap { |a| a[5] = d }.join.to_i

        return if num > $finish
        $counter += 1
        puts b_digs.tap { |a| a[5] = d }.join
      end
    else
      $counter += 1 if range.member?(prev_dig)
      puts b_digs.tap { |a| a[5] = prev_dig }.join
    end

    return
  end

  range.each do |i|
    b_digs[lvl] = i
    unless $start
      (lvl+1..5).each { |ind| b_digs[ind] = 0 }
    end

    checker(
      b_digs: b_digs,
      e_digs: e_digs,
      double_met: double_met || prev_dig == i,
      prev_dig: i,
      lvl: lvl + 1,
      finish: finish && b_chars[lvl] == e_chars[lvl]
    )
  end
end

checker(b_digs: split(start), e_digs: split($finish), double_met: false, prev_dig: 0, lvl: 0, finish: false)

puts $counter
