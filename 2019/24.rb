input = <<-STR
<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>
STR

input = <<-STR
<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>
STR

input = <<-STR
<x=-6, y=-5, z=-8>
<x=0, y=-3, z=-13>
<x=-15, y=10, z=-11>
<x=-3, y=-8, z=3>
STR

require 'byebug'

lines = input.split("\n")
satelites = lines.map do |coord_str|
  coord_str.split(', ').map { |c| c.split('=').last.to_i }
end

class Sat
  attr_reader :coords, :vel, :period, :vel_period, :period_confirmed, :vel_period_confirmed

  def initialize(coords, index = 0)
    @coords = coords
    @vel = [0, 0, 0]
    @index = index

    @initial = coords.clone
    @initial_vel = [0, 0, 0]
    @period = [nil, nil, nil]
    @period_confirmed = [nil, nil, nil]
    @vel_period_confirmed = [nil, nil, nil]
    @vel_period = [nil, nil, nil]
  end

  def apply_gravity(sat)
    shift = [coords, sat.coords].transpose.map do |pair|
      if pair[0] < pair[1]
        [1, -1]
      elsif pair[1] < pair[0]
        [-1, 1]
      else
        [0, 0]
      end
    end

    adjust = shift.transpose
    add_velocity(adjust.first)
    sat.add_velocity(adjust.last)
  end

  def add_velocity(vector)
    3.times do |i|
      @vel[i] += vector[i]
    end
  end

  def apply_velocity(time = 0)
    3.times do |i|
      @coords[i] += vel[i]
      # if @coords[i] == @initial[i]
      #   @period[i] ||= []
      #   @period[i] << time
      #   puts "Periods #{@index}:#{i} - #{@period[i]}"

      #   if conf = @period[i].find { |p| time % p == 0 && time / p >= 2 }
      #     @period_confirmed[i] ||= conf
      #     # puts "Periods for #{@index}: #{@period_confirmed}"
      #   end
      # else
      #   # (@period[i] || []).reject! { |p| time % p == 0 }
      #   # @period_confirmed[i] = nil if @period_confirmed[i] && time % @period_confirmed[i] == 0
      # end
    end
  end

  def to_s
    "POS: <x:\t#{coords[0]}, y:\t#{coords[1]},\tz: #{coords[2]}>. VEL <x:\t#{vel[0]}, y:\t#{vel[1]},\tz: #{vel[2]}>. E: #{energy}"
  end

  def energy
    potential * kinetic
  end

  def potential
    coords.map(&:abs).inject(&:+)
  end

  def kinetic
    vel.map(&:abs).inject(&:+)
  end

  def record_vel_period(time)
    # 3.times do |i|
    #   if @vel[i] == @initial_vel[i]
    #     @vel_period[i] ||= []
    #     @vel_period[i] << time
    #     if conf = @vel_period[i].find { |p| time % p == 0 && time / p >= 2 }
    #       @vel_period_confirmed[i] ||= conf
    #       # puts "Velocity periods for #{@index}: #{@vel_period_confirmed}"
    #     end
    #   else
    #     (@vel_period[i] || []).reject! { |p| time % p == 0 }
    #     @vel_period_confirmed[i] = nil if @vel_period_confirmed[i] && time % @vel_period_confirmed[i] == 0
    #   end
    # end
  end

  def initial?
    coords == @initial && vel == @initial_vel
  end

  def c_initial?(i)
    coords[i] == @initial[i]
  end

  def v_initial?(i)
    vel[i] == @initial_vel[i]
  end

  def periods_found?
    @period.all? && @vel_period.all?
  end

  def periods_confirmed?
    @period_confirmed.all? && @vel_period_confirmed.all?
  end
end

steps = 1000

sats = []
satelites.each_with_index do |s, i|
  sats << Sat.new(s, i)
end

def interact(sats)
  sats[1..-1].each do |other|
    sats[0].apply_gravity(other)
  end

  interact(sats[1..-1]) unless sats[1..-1].empty?
end

puts "Step 0"
sats.each do |s|
  puts s.to_s
end

i = 1

axis_periods = [nil, nil, nil]

loop do
  puts "Step #{i + 1}" if (i + 1) % 1000000 == 0

  interact(sats)

  sats.each_with_index do |s, ind|
    s.apply_velocity(i)
  end

  axises = 3.times.to_a.select do |axis|
    sats.all? do |s|
      s.c_initial?(axis) && s.v_initial?(axis)
    end
  end

  unless axises.empty?
    axises.each do |axis|
      puts "Axis #{axis} = #{i}"
      axis_periods[axis] ||= i
    end
  end

  break if axis_periods.all?

  break if sats.all?(&:periods_found?) && sats.all?(&:periods_confirmed?) && stats.map(&:period_confirmed).flatten.max * 2 < i && stats.map(&:vel_period_confirmed).flatten.max * 2 < i || sats.all?(&:initial?)
  i += 1
end

puts axis_periods.inspect

puts axis_periods[0].lcm(axis_periods[1]).lcm(axis_periods[2])
