require 'byebug'

input = [3,8,1005,8,315,1106,0,11,0,0,0,104,1,104,0,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,0,10,4,10,101,0,8,29,2,1006,16,10,3,8,102,-1,8,10,1001,10,1,10,4,10,1008,8,0,10,4,10,102,1,8,55,3,8,102,-1,8,10,1001,10,1,10,4,10,108,1,8,10,4,10,101,0,8,76,1,101,17,10,1006,0,3,2,1005,2,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,1,10,4,10,101,0,8,110,1,107,8,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,108,0,8,10,4,10,101,0,8,135,1,108,19,10,2,7,14,10,2,104,10,10,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,1,10,4,10,101,0,8,170,1,1003,12,10,1006,0,98,1006,0,6,1006,0,59,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,0,10,4,10,102,1,8,205,1,4,18,10,1006,0,53,1006,0,47,1006,0,86,3,8,1002,8,-1,10,101,1,10,10,4,10,108,0,8,10,4,10,1001,8,0,239,2,9,12,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,1,10,4,10,101,0,8,266,1006,0,8,1,109,12,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,108,1,8,10,4,10,1001,8,0,294,101,1,9,9,1007,9,1035,10,1005,10,15,99,109,637,104,0,104,1,21102,936995730328,1,1,21102,1,332,0,1105,1,436,21102,1,937109070740,1,21101,0,343,0,1106,0,436,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,21102,1,179410308187,1,21101,0,390,0,1105,1,436,21101,0,29195603035,1,21102,1,401,0,1106,0,436,3,10,104,0,104,0,3,10,104,0,104,0,21102,825016079204,1,1,21102,1,424,0,1105,1,436,21102,1,825544672020,1,21102,435,1,0,1106,0,436,99,109,2,21202,-1,1,1,21102,1,40,2,21102,467,1,3,21101,0,457,0,1105,1,500,109,-2,2106,0,0,0,1,0,0,1,109,2,3,10,204,-1,1001,462,463,478,4,0,1001,462,1,462,108,4,462,10,1006,10,494,1102,0,1,462,109,-2,2106,0,0,0,109,4,1202,-1,1,499,1207,-3,0,10,1006,10,517,21102,1,0,-3,22101,0,-3,1,22101,0,-2,2,21101,1,0,3,21101,0,536,0,1106,0,541,109,-4,2106,0,0,109,5,1207,-3,1,10,1006,10,564,2207,-4,-2,10,1006,10,564,21202,-4,1,-4,1105,1,632,21202,-4,1,1,21201,-3,-1,2,21202,-2,2,3,21101,583,0,0,1106,0,541,22102,1,1,-4,21101,0,1,-1,2207,-4,-2,10,1006,10,602,21101,0,0,-1,22202,-2,-1,-2,2107,0,-3,10,1006,10,624,21202,-1,1,1,21101,624,0,0,106,0,499,21202,-2,-1,-2,22201,-4,-2,-4,109,-5,2106,0,0]

class Intcode
  attr_reader :program, :inputs, :outputs, :relative_base
  attr_accessor :position

  def initialize(program)
    @program = program
    @inputs = []
    @position = 0
    @halt = false
    @outputs = []
    @relative_base = 0
  end

  def add_input(inp)
    @inputs << inp
  end

  def halt?
    @halt
  end

  def output
    @outputs.last
  end

  def run!
    loop do
      cmd, mode1, mode2, mode3 = commands(program[position])

      case cmd
      when 1 # add
        new_value(
          position + 3,
          mode3,
          value(position + 1, mode1) + value(position + 2, mode2)
        )
        @position += 4
      when 2 # multiply
        new_value(
          position + 3,
          mode3,
          value(position + 1, mode1) * value(position + 2, mode2)
        )
        @position += 4
      when 3 # write
        new_value(
          position + 1,
          mode1,
          inputs.shift
        )
        @position += 2
      when 4 # output
        @outputs << value(position + 1, mode1)
        @position += 2
        return output
      when 5 # jump-if-true
        if value(position + 1, mode1) != 0
          @position = value(position + 2, mode2)
        else
          @position += 3
        end
      when 6 # jump-if-false
        if value(position + 1, mode1).zero?
          @position = value(position + 2, mode2)
        else
          @position += 3
        end
      when 7 # less-than
        if value(position + 1, mode1) < value(position + 2, mode2)
          new_value(position + 3, mode3, 1)
        else
          new_value(position + 3, mode3, 0)
        end
        @position += 4
      when 8 # equals
        if value(position + 1, mode1) == value(position + 2, mode2)
          new_value(position + 3, mode3, 1)
        else
          new_value(position + 3, mode3, 0)
        end
        @position += 4
      when 9 # relative_base
        @relative_base += value(position + 1, mode1)
        @position += 2
      when 99 # halt
        @position += 1
        @halt = true
        return output
      else
        raise "Error at #{position}"
      end
    end
  end

  def value(pos, mode)
    case mode
    when 0
      program[program[pos]] || 0
    when 1
      program[pos] || 0
    when 2
      program[program[pos] + relative_base] || 0
    end
  end

  def new_value(pos, mode, value)
    case mode
    when 0
      program[program[pos]] = value
    when 1
      program[pos] = value
    when 2
      program[program[pos] + relative_base] = value
    end
  end

  def commands(value)
    command = value % 100
    mode1 = (value / 100) % 10
    mode2 = (value / 1000) % 10
    mode3 = (value / 10_000) % 10

    [command, mode1, mode2, mode3]
  end
end

points = {}

prog = Intcode.new(input.clone)

current = [0,0]

current_dir = 0
directions = [:up, :left, :down, :right]

def draw(points, current, direction)
  (-10..10).each do |x|
    (-10..50).each do |y|
      if current == [x, y]
        res = case direction
        when :up then '^'
        when :down then 'v'
        when :left then '<'
        when :right then '>'
        end
        print res
      else
        color = points[[x,y]]
        print(color == 0 ? '#' : '.')
      end
    end
    puts
  end
end

start = 1

loop do
  # draw(points, current, directions[current_dir])
  # gets
  puts "POS: #{current}"
  puts "Color: #{points[current] || 0}"
  break if prog.halt?

  if start
    prog.add_input(start)
    start = nil
  else
    prog.add_input(points[current] || 0)
  end

  color = prog.run!
  direction = prog.run!
  puts "Paint #{color}"
  puts "Current direction #{directions[current_dir]}"
  puts "Direction #{direction == 0 ? 'turn left' : 'turn right' }"

  points[current.clone] = color

  case direction
  when 0
    current_dir += 1
    current_dir = current_dir % 4
  when 1
    current_dir -= 1
    current_dir = current_dir % 4
  else
    raise
  end

  case directions[current_dir]
  when :up
    current[0] -= 1
  when :down
    current[0] += 1
  when :left
    current[1] -= 1
  when :right
    current[1] += 1
  else
    raise
  end
end

draw(points, current, directions[current_dir])
puts points.keys.count
