numbers = File.read('res.txt').split

require 'byebug'

def valid?(n)
  digs = n.chars.map(&:to_i)

  prev = 0
  double = false

  groups = {}

  digs.each do |d|
    groups[d] ||= 0
    groups[d] += 1

    double = true if prev == d
    return false unless prev <= d
    prev = d
  end

  return false unless groups.values.any? { |c| c == 2 }

  return double
end


count = 0
numbers.each do |n|
  v = valid?(n)
  count += 1 if v
  puts "#{v ? '+' : '='} #{n}"
end


puts count
