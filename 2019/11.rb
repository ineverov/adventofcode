input = File.read('11.txt').split

# input = <<-TEXT.split
# COM)B
# B)C
# C)D
# D)E
# E)F
# B)G
# G)H
# D)I
# E)J
# J)K
# K)
# TEXT

$satelits = {}

input.each do |orb|
  left, right = orb.split(')')
  $satelits[left] ||= []
  $satelits[left] << right
end

class Tree
  def initialize(val)
    @value = val
    @leafs = []
  end

  def add_nodes(sats)
    @leafs = sats.map{ |s| Tree.new(s) }
  end
end

require 'byebug'

def count(node, total)
  ($satelits[node] || []).inject(total) do |sum, sub|
    sum + count(sub, total + 1)
  end
end

puts count('COM', 0)

